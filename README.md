## Chord Lister with MPI

This was a school project using MPI and GMP on the school cluster. As a semester project I chose to write a program which enumerates all the musical chord classes in a given equal temperament (usually microtonal), then discovers which chords are "adjacent" to which. This can serve as a guide to the chord space.

The file [README.html](README.html) was written to accompany a presentation on the project.