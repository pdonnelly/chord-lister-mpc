chords: *.c
	mpicc -std=c99 -g -o chords main.c -lgmp -lm

clean:
	rm chords
