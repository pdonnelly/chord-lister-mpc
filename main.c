#include <stdio.h>
#include <stdarg.h>
#include <gmp.h>
#include <malloc.h>
#include <math.h>
#include <mpi.h>
#include <time.h>

// sorry
#define DO(times) for(int i=0; i<(times); ++i)

int rank;

int edo=22; // Size of the edo we're working in.
int nchord=4; // What chord size to calculate for.

// Transpose up by one step of the edo. Would be better if we didn't
// init q and r... but not good to pass them so maybe I'd better
// inline this code.
void xpose1(mpz_t rop) {
  mpz_t q,r; mpz_init(q); mpz_init(r);
  mpz_mul_2exp(rop,rop,1);
  mpz_tdiv_q_2exp(q,rop,edo); mpz_tdiv_r_2exp(r,rop,edo);
  mpz_ior(rop,q,r);
  mpz_clear(q); mpz_clear(r);
}

int csize(const mpz_t c) { return mpz_popcount(c); }

// Are chords A and B transpositionally equivalent?
int teq(const mpz_t a, const mpz_t b) {
  if(csize(a) != csize(b)) goto not_equal;
  
  mpz_t c; mpz_init_set(c,b);
  DO(edo)if(mpz_cmp(a,c)) xpose1(c); else goto equal;
  mpz_clear(c);

 not_equal:
  return 0;
 equal:
  mpz_clear(c);
  return 1;
}

int find_teq(const mpz_t chord, mpz_t *table, const int length) {
  DO(length)if(teq(chord,table[i])) return i;
  return -1;
}

void combinations(mpz_t result, const unsigned int n, const unsigned int k) {
  mpz_set_ui(result,n);
  for(int i=1; i< k; ++i) mpz_mul_ui(result,result,n-i);
  for(int i=2; i<=k; ++i) mpz_tdiv_q_ui(result,result,i);
}

struct nchordlist { int length; int num_used; mpz_t *chords; };
struct nchordlist make_nchordlist(int length) {
  struct nchordlist list;
  list.num_used=0;
  list.length=length;
  list.chords=malloc(length*sizeof(mpz_t));
  DO(length)mpz_init(list.chords[i]);
  return list;
}
void free_nchordlist(struct nchordlist l) {
  DO(l.length)mpz_clear(l.chords[i]);
  free(l.chords);
}

struct nchordlist list_nchords(int chord_size) {
  // Calculate some kind of upper bound on the number of chord classes
  // of this size, so we can preallocate some storage space for them.
  mpz_t b; mpz_init(b);
  combinations(b,edo,chord_size);
  unsigned int bound=mpz_get_ui(b);
  size_t space=bound*sizeof(mpz_t);
  struct nchordlist l=make_nchordlist(bound);
  
  mpz_t probe; mpz_init_set_ui(probe,1);
  mpz_t panchord; mpz_init_set_ui(panchord,1);
  mpz_mul_2exp(panchord,panchord,edo); mpz_sub_ui(panchord,panchord,1);
  for(; 1>mpz_cmp(probe,panchord); mpz_add_ui(probe,probe,1) )
    if(csize(probe) == chord_size
       && find_teq(probe,l.chords,l.num_used) == -1)
      mpz_set(l.chords[l.num_used++],probe);

  mpz_clear(b);
  return l;
}

int apply_voice_leading(mpz_t result, const mpz_t source,
                         int *vl, int vlnotes) {
  if(csize(source)!=vlnotes) {
    printf(" size mismatch\n");
    return -1;
  } 

  // convert source to tuple of scale degrees
  int notes[edo]; // just go big?
  int scanstart=-1;
  DO(vlnotes)notes[i]=scanstart=mpz_scan1(source,scanstart+1);

  // apply vl
  DO(vlnotes)printf("%3d", notes[i]); printf(" to ");
  DO(vlnotes){notes[i]+=vl[i]; if(notes[i]<0)notes[i]+=edo;}
  DO(vlnotes)printf("%3d", notes[i]);

  // convert back and store in result
  mpz_set_ui(result,0);
  DO(vlnotes)mpz_setbit(result,notes[i]%edo);

  return csize(result);
}

void printch(const mpz_t chord) { // Print a chord in "binary"
  DO(edo)if(mpz_tstbit(chord,i))printf("%d",i%10);else printf(".");
}

void list_voice_leadings(struct nchordlist l, int chord_size,
                         int start_index, int below_index) {
    mpz_t res; mpz_init(res);

    int vl[edo];DO(chord_size)vl[i]=-1;
    for(int c=start_index; c<below_index; ++c) {
      DO(pow(3,chord_size)){
        printf("R%d#%d:",rank,c);
        DO(chord_size)printf("%3d", vl[i]); printf(" takes");
        int size=apply_voice_leading(res,l.chords[c],vl,chord_size);
        int index=find_teq(res,l.chords,l.num_used);
        if(index>=0) {
          gmp_printf(" which is #%d ", index);
          printch(l.chords[index]);
        }

        printf("\n");
        // Increment to next vl
        ++vl[0];for(int i=0;vl[i]>1; ++i) {vl[i]=-1;++vl[i+1];}
      }
    }
    
    printf("counted %d %d-chords\n", below_index-start_index, chord_size);
    mpz_clear(res);
}

int main(int argc, char **argv) {
  clock_t start_time = clock();
  int num_procs;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

  if(rank==0) gmp_printf("In %dedo\n", edo);

  // I don't know how to parallelize this, so might as well duplicate
  // the work to avoid having to send it around.
  struct nchordlist l=list_nchords(nchord);

  int chunk_size=l.num_used/num_procs;
  // Make the last proc get the remainder.
  int start=chunk_size*rank;
  int below=(rank<num_procs-1)?(chunk_size*(rank+1)):l.num_used;

  printf("Rank %d/%d taking #s %d below %d.\n", rank, num_procs, start, below);
  list_voice_leadings(l,nchord,start,below);

  free_nchordlist(l);

  MPI_Barrier(MPI_COMM_WORLD);
  if(rank==0) {
    clock_t end_time = clock();
    printf("Total time was %f seconds.\n",
           ((double)(end_time-start_time))/CLOCKS_PER_SEC);
  }
  
  MPI_Finalize();
  return 0;
}
